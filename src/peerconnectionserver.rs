use futures::{SinkExt, StreamExt};
use serde::{Deserialize, Serialize};
use std::convert::TryFrom;
use tokio::sync::{mpsc, oneshot};
use warp::ws::{Message, WebSocket};
use warp::Filter;

use gst::glib;
use gst::prelude::*;

pub struct WebServer {
    stop_tx: tokio::sync::oneshot::Sender<()>,
    thread_handle: std::thread::JoinHandle<()>,
}

impl WebServer {
    pub fn new(
        routes: warp::filters::BoxedFilter<(impl warp::Reply + 'static,)>,
        port: u16,
    ) -> Self {
        let (stop_tx, stop_rx) = oneshot::channel();

        let thread_handle = std::thread::spawn(move || {
            let runtime = tokio::runtime::Builder::new_current_thread()
                .enable_io().build().unwrap();
            runtime.block_on(async {
                let server = create_server(routes, port, stop_rx);
                server.await;
            });
        });

        WebServer {
            stop_tx,
            thread_handle,
        }
    }

    pub fn stop(self: WebServer) {
        self.stop_tx.send(()).ok();
        self.thread_handle.join().unwrap();
    }
}

fn create_server(
    routes: warp::filters::BoxedFilter<(impl warp::Reply + 'static,)>,
    port: u16,
    stop_rx: oneshot::Receiver<()>,
) -> impl warp::Future<Output = ()> {
    let (_, server) =
        warp::serve(routes).bind_with_graceful_shutdown(([0, 0, 0, 0], port), async move {
            stop_rx.await.ok();
        });

    server
}

pub fn create_routes(webrtcbin: gst::Element) -> warp::filters::BoxedFilter<(impl warp::Reply,)> {
    let indexhtml = std::include_str!("index.html");
    let indexpath = warp::path::end().map(move || warp::reply::html(indexhtml));

    let rtcjsfile = std::include_str!("remote-rtc-peer-connection.js");
    let rtcjspath = warp::path("remote-rtc-peer-connection.js")
        .map(move || warp::reply::with_header(rtcjsfile, "content-type", "text/javascript"));

    let sdpjsfile = std::include_str!("sdp.js");
    let sdpjspath = warp::path("sdp.js")
        .map(move || warp::reply::with_header(sdpjsfile, "content-type", "text/javascript"));

    let wspath = warp::path("webrtcbin")
        .and(warp::ws())
        .map(move |ws: warp::ws::Ws| {
            let webrtcbin = webrtcbin.clone();
            ws.on_upgrade(move |websocket| handle_websocket(websocket, webrtcbin))
        });

    wspath.or(indexpath).or(rtcjspath).or(sdpjspath).boxed()
}

async fn handle_websocket(websocket: WebSocket, webrtcbin: gst::Element) {
    let (tx, mut rx) = mpsc::unbounded_channel();

    let (mut ws_sink, mut ws_stream) = websocket.split();

    tokio::task::spawn(async move {
        while let Some(reply) = rx.recv().await {
            if ws_sink.send(reply).await.is_err() {
                break;
            };
        }
        ws_sink.close().await.ok();
    });

    for spec in webrtcbin.list_properties().iter() {
        let name = spec.name();
        let prop = webrtcbin.property::<glib::Value>(name);
        if let Ok(prop) = prop.serialize() {
            send_property(&name, prop.to_string(), &tx);
        }
    }

    let tx2 = tx.clone();
    webrtcbin.connect_notify(None, move |webrtcbin, spec| {
        let name = spec.name();
        let prop = webrtcbin.property::<glib::Value>(name);
        if let Ok(prop) = prop.serialize() {
            send_property(&name, prop.to_string(), &tx2);
        } else {
            send_property(&name, "nil".to_string(), &tx2);
        }
    });

    let tx2 = tx.clone();
    webrtcbin
        .connect("on-negotiation-needed", false, move |_| {
            send_event(WebEvent::OnNegotiationNeeded, &tx2);
            None
        });

    let tx2 = tx.clone();
    webrtcbin
        .connect("on-ice-candidate", false, move |values| {
            let mlineindex = values[1].get::<u32>().expect("Invalid argument");
            let candidate = values[2]
                .get::<String>()
                .expect("Invalid argument");
            let cand = RTCIceCandidate {
                candidate,
                sdp_mline_index: u16::try_from(mlineindex).unwrap_or(0),
            };
            send_event(WebEvent::OnIceCandidate(Some(cand)), &tx2);
            None
        });

    while let Some(Ok(message)) = ws_stream.next().await {
        if !message.is_text() {
            return;
        }
        if let Err(err) = handle_message(message, tx.clone(), &webrtcbin) {
            println!("Wrong message, closing connection: {:?}", err);
            return;
        }
    }
    println!("Socket disconnected");
}

#[derive(Serialize, Deserialize, Debug)]
#[serde(tag = "request", content = "object")]
enum RequestType {
    CreateOffer,
    CreateAnswer,
    SetLocalDescription(RTCSessionDescription),
    SetRemoteDescription(RTCSessionDescription),
    AddIceCandidate(Option<RTCIceCandidate>),
}

#[derive(Serialize, Deserialize, Debug)]
struct WebRequest {
    id: String,
    #[serde(flatten)]
    request: RequestType,
}

#[derive(Serialize, Deserialize, Debug)]
struct WebReply {
    id: String,
    success: bool,
    value: String,
}

#[derive(Serialize, Deserialize, Debug, Clone)]
#[serde(rename_all = "lowercase")]
enum WebRTCSDPType {
    Offer,
    Pranswer,
    Answer,
    Rollback,
}

#[derive(Serialize, Deserialize, Debug, Clone)]
struct RTCSessionDescription {
    #[serde(rename = "type")]
    type_: WebRTCSDPType,
    sdp: String,
}

#[derive(Serialize, Deserialize, Debug)]
struct RTCIceCandidate {
    candidate: String,
    #[serde(rename = "sdpMLineIndex", default)]
    sdp_mline_index: u16,
}

#[derive(Serialize, Deserialize, Debug)]
#[serde(rename_all = "lowercase", tag = "event", content = "object")]
enum WebEvent {
    OnNegotiationNeeded,
    OnIceCandidate(Option<RTCIceCandidate>),
}

#[derive(Serialize, Deserialize, Debug)]
struct WebProperty {
    property: String,
    value: String,
}

#[derive(Serialize, Debug)]
struct DOMException<'a> {
    name: &'a str,
    message: &'a str
}

type TxMessage = mpsc::UnboundedSender<Message>;

fn send_reply(id: String, value: String, tx: TxMessage) {
    let reply = WebReply {
        id,
        success: true,
        value,
    };
    let reply = serde_json::to_string(&reply).unwrap();
    tx.send(Message::text(reply)).ok();
}

fn send_error(id: String, name: &str, message: &str, tx: TxMessage) {
    let exception = DOMException{name, message};
    let exception = serde_json::to_string(&exception).unwrap();
    let reply = WebReply {
        id,
        success: false,
        value: exception,
    };
    let reply = serde_json::to_string(&reply).unwrap();
    tx.send(Message::text(reply)).ok();
}

fn send_event(event: WebEvent, tx: &TxMessage) {
    let event = serde_json::to_string(&event).unwrap();
    tx.send(Message::text(event)).ok();
}

fn send_property(property: &str, value: String, tx: &TxMessage) {
    let prop = WebProperty {
        property: property.to_string(),
        value,
    };
    let prop = serde_json::to_string(&prop).unwrap();
    tx.send(Message::text(prop)).ok();
}

fn create_offer(id: String, tx: TxMessage, webrtcbin: &gst::Element) {
    println!("Got request for offer {}", id);

    let promise = gst::Promise::with_change_func(move |reply| match reply {
        Ok(Some(s)) => {
            if s.name() == "application/x-gst-promise" {
                let offer = s
                    .value("offer")
                    .expect("Invalid content of promise")
                    .get::<gst_webrtc::WebRTCSessionDescription>()
                    .unwrap();
                let sdp = offer.sdp();
                let value = RTCSessionDescription {
                    type_: WebRTCSDPType::Offer,
                    sdp: sdp.to_string(),
                };
                let value = serde_json::to_string(&value).unwrap();
                send_reply(id, value, tx);
            }
        }
        Err(gst::PromiseError::Interrupted) => send_error(id, "interrupted", "interrupted", tx),
        Err(_) => send_error(id, "other-error", "other-error", tx),
        Ok(None) => panic!("Unexpected None"),
    });

    webrtcbin
        .emit_by_name::<()>("create-offer", &[&None::<gst::Structure>, &promise]);
}

fn create_answer(id: String, tx: TxMessage, webrtcbin: &gst::Element) {
    println!("Got request for answer {}", id);

    let promise = gst::Promise::with_change_func(move |reply| match reply {
        Ok(Some(s)) => {
            if s.name() == "application/x-gst-promise" {
                let offer = s
                    .value("answer")
                    .expect("Invalid content of promise")
                    .get::<gst_webrtc::WebRTCSessionDescription>()
                    .unwrap();
                let sdp = offer.sdp();
                let value = RTCSessionDescription {
                    type_: WebRTCSDPType::Answer,
                    sdp: sdp.to_string(),
                };
                let value = serde_json::to_string(&value).unwrap();
                send_reply(id, value, tx);
            }
        }
        Err(gst::PromiseError::Interrupted) => send_error(id, "interrupted", "interrupted", tx),
        Err(_) => send_error(id, "other-error", "other-error", tx),
        Ok(None) => panic!("Unexpected None"),
    });

    webrtcbin
        .emit_by_name::<()>("create-answer", &[&None::<gst::Structure>, &promise]);
}


#[derive(Clone,Copy,Debug,Eq,PartialEq)]
#[repr(i32)]
#[allow(dead_code)]
enum GstWebRtcError {
    Failed = 0,
    InvalidSyntax,
    InvalidModification,
    InvalidState,
    BadSdp,
    Fingerprint,
    SctpFailure,
    DataChannelFailure,
    Closed,
    NotImplemented
}

impl glib::error::ErrorDomain for GstWebRtcError {
    fn domain() -> glib::Quark {
        glib::Quark::from_str("gst-webrtc-bin-error-quark")
    }
    fn code(self) -> i32 {
        self as i32
    }
    fn from(code: i32) -> Option<Self> {
        let max = GstWebRtcError::NotImplemented as i32;

        if code < max {
            let code: GstWebRtcError = unsafe { ::std::mem::transmute(code) };
            Some(code)
        } else {
            None


        }
    }
}

fn set_description(
    id: String,
    api: &str,
    description: RTCSessionDescription,
    tx: TxMessage,
    webrtcbin: &gst::Element,
) {
    let parsed_sdp = match gst_sdp::SDPMessage::parse_buffer(&description.sdp.as_bytes()) {
        Ok(v) => v,
        Err(_) => {
            send_error(id, "parsing-error", "Couldn't parse SDP", tx);
            return;
        }
    };

    let promise = gst::Promise::with_change_func(move |reply| match reply {
        Ok(Some(s)) => {
            if s.name() == "application/x-gstwebrtcbin-error"
             || s.name() == "application/x-getwebrtcbin-error" {
                let err = s.value("error")
                .expect("Error is missing GError")
                .get::<glib::Error>().unwrap();

                if let Some(gerr) = err.kind::<GstWebRtcError>() {
                    match gerr {
                        GstWebRtcError::InvalidState => send_error(id, "InvalidStateError", &err.to_string(), tx),
                        GstWebRtcError::InvalidSyntax => send_error(id, "OperationError", &err.to_string(), tx),
                        GstWebRtcError::BadSdp => send_error(id, "OperationError", &err.to_string(), tx),
                        GstWebRtcError::InvalidModification => send_error(id, "OperationError", &err.to_string(), tx),
                        GstWebRtcError::Fingerprint  => send_error(id, "OperationError", &err.to_string(), tx),
                        _ => send_error(id, "unknown-error", &err.to_string(), tx)
                    }
                } else {
                        send_error(id, "unknown-error", &err.to_string(), tx);
                }

            } else {
                panic!("Unexpected value: {:?}", s);
            }
        },
        Err(gst::PromiseError::Interrupted) => send_error(id, "interrupted", "interrupted", tx),
        Err(_) => send_error(id, "other-error", "other-error" , tx),
        Ok(None) => {
            send_reply(id, "{}".to_string(), tx);
        }
    });

    let type_: gst_webrtc::WebRTCSDPType = match description.type_ {
        WebRTCSDPType::Offer => gst_webrtc::WebRTCSDPType::Offer,
        WebRTCSDPType::Pranswer => gst_webrtc::WebRTCSDPType::Pranswer,
        WebRTCSDPType::Answer => gst_webrtc::WebRTCSDPType::Answer,
        WebRTCSDPType::Rollback => gst_webrtc::WebRTCSDPType::Rollback,
    };

    let gstdesc = gst_webrtc::WebRTCSessionDescription::new(type_, parsed_sdp);

    webrtcbin.emit_by_name::<()>(api, &[&gstdesc, &promise]);
}

fn add_ice_candidate(
    id: String,
    candidate: Option<RTCIceCandidate>,
    tx: TxMessage,
    webrtcbin: &gst::Element,
) {
    println!("Got new ice candidate {}", id);

    match candidate {
        None => {
            const MLINE_INDEX: u32 = 0;
            println!("Adding empty cand");
            webrtcbin
                .emit_by_name::<()>("add-ice-candidate", &[&MLINE_INDEX, &""]);
        }
        Some(RTCIceCandidate {
            candidate,
            sdp_mline_index,
        }) => {
            let sdp_mline_index = sdp_mline_index as u32;

            webrtcbin
                .emit_by_name::<()>("add-ice-candidate", &[&sdp_mline_index, &candidate]);
        }
    }

    send_reply(id, "true".to_string(), tx);
}

fn handle_message(
    message: Message,
    tx: TxMessage,
    webrtcbin: &gst::Element,
) -> Result<(), Box<dyn std::error::Error>> {
    let message = message.to_str().unwrap();
    let request: WebRequest = serde_json::from_str(message)?;

    match request {
        WebRequest {
            id,
            request: RequestType::CreateOffer,
        } => create_offer(id, tx, webrtcbin),
        WebRequest {
            id,
            request: RequestType::CreateAnswer,
        } => create_answer(id, tx, webrtcbin),
        WebRequest {
            id,
            request: RequestType::SetLocalDescription(description),
        } => set_description(id, "set-local-description", description, tx, webrtcbin),
        WebRequest {
            id,
            request: RequestType::SetRemoteDescription(description),
        } => set_description(id, "set-remote-description", description, tx, webrtcbin),
        WebRequest {
            id,
            request: RequestType::AddIceCandidate(candidate),
        } => add_ice_candidate(id, candidate, tx, webrtcbin),
    }

    Ok(())
}

#[cfg(test)]
mod tests {
    use super::*;

    struct Harness {
        pipeline: gst::Element,
    }

    impl Harness {
        fn new_sink() -> (Self, warp::filters::BoxedFilter<(impl warp::Reply + 'static,)>    ) {
            let pipeline =
                gst::parse_launch("audiotestsrc ! alawenc ! rtppcmapay ! webrtcbin name=rtcbin")
                    .unwrap();

            pipeline.set_state(gst::State::Playing).unwrap();

            let webrtcbin = pipeline
                .clone()
                .downcast::<gst::Bin>()
                .unwrap()
                .by_name("rtcbin")
                .unwrap();

            let routes = create_routes(webrtcbin);

            (Harness {pipeline}, routes)
        }
        fn stop(self: Harness) {
            self.pipeline.set_state(gst::State::Null).unwrap();
        }
    }

    #[tokio::test]
    async fn regular_files() {
        gst::init().unwrap();

        let (harness, routes) = Harness::new_sink();

        let index = warp::test::request().reply(&routes).await;

        assert_eq!(index.status(), 200);

        let js = warp::test::request().path("/remote-rtc-peer-connection.js").reply(&routes).await;

        assert_eq!(js.status(), 200);
        assert_eq!(js.headers()[http::header::CONTENT_TYPE], "text/javascript");

        let sdpjs = warp::test::request().path("/sdp.js").reply(&routes).await;

        assert_eq!(sdpjs.status(), 200);
        assert_eq!(sdpjs.headers()[http::header::CONTENT_TYPE], "text/javascript");


        harness.stop();
    }

    #[derive(Deserialize, Debug)]
    #[serde(untagged)]
    enum TestMessage {
        Event(WebEvent),
        Property(WebProperty),
        Reply(WebReply)
    }

    async fn recv_reply(ws: &mut warp::test::WsClient, id: &str) -> WebReply {
        loop {
            let message = ws.recv().await.expect("Got message");
            assert!(message.is_text());
            
            let message = message.to_str().unwrap();

            let message: TestMessage = serde_json::from_str(&message).unwrap();
            
            match message {
                TestMessage::Event(e) => println!("Got {:#?}", e),
                TestMessage::Property(p) => println!("Got {:#?}", p),
                TestMessage::Reply(r) => {
                    assert_eq!(r.id, id);
                    return r;
                }
            }
        }
    }

    async fn build_websocket() -> (Harness, warp::test::WsClient){
        let (harness, routes) = Harness::new_sink();

        let index = warp::test::request().reply(&routes).await;

        assert_eq!(index.status(), 200);

        let ws = warp::test::ws().path("/webrtcbin").handshake(routes).await.expect("handshake");

        (harness, ws)
    }

    #[tokio::test]
    async fn create_and_set_desc() {
        gst::init().unwrap();

        let (harness, mut ws) = build_websocket().await;

        ws.send_text("{\"id\": \"1\", \"request\": \"CreateOffer\" }").await;
        let r = recv_reply(&mut ws, "1").await;
        assert!(r.success);
        let desc: RTCSessionDescription = serde_json::from_str(&r.value).unwrap();
        println!("offer: {:#?}", desc);

        let req = WebRequest {id: "2".to_string(), request: RequestType::SetLocalDescription(desc.clone())};
        let req = serde_json::to_string(&req).unwrap();
        ws.send_text(req).await;
        let r = recv_reply(&mut ws, "2").await;
        assert!(r.success);
        assert_eq!(r.value, "{}");

        let desc = RTCSessionDescription { type_: WebRTCSDPType::Answer,
              sdp: desc.sdp.replace("a=setup:actpass", "a=setup:passive")};
        let req = WebRequest {id: "3".to_string(), request: RequestType::SetRemoteDescription(desc)};
        let req = serde_json::to_string(&req).unwrap();
        ws.send_text(req).await;
        let r = recv_reply(&mut ws, "3").await;
        assert!(r.success);
        assert_eq!(r.value, "{}");

        harness.stop();
    }

    #[tokio::test]
    async fn invalid_offer() {
        gst::init().unwrap();

        let (harness, mut ws) = build_websocket().await;

        let desc = RTCSessionDescription {type_: WebRTCSDPType::Answer, sdp:
            "v=2\nblahblahlbha\nm=video 312312 AA/AA 2 4\nsdasas\ndsad\n".to_string()};
        let req = WebRequest {id: "3".to_string(), request: RequestType::SetRemoteDescription(desc)};
        let req = serde_json::to_string(&req).unwrap();
        ws.send_text(req).await;
        let r = recv_reply(&mut ws, "3").await;
        assert!(!r.success);

        harness.stop();
    }
}
