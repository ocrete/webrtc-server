use gst::glib;

mod peerconnectionserver;

mod webrtcsink;
mod webrtcsrc;

gst::plugin_define!(
        webrtcserver,
        env!("CARGO_PKG_DESCRIPTION"),
        plugin_init,
        concat!(env!("CARGO_PKG_VERSION"), "-", env!("COMMIT_ID")),
        "LGPL",
        env!("CARGO_PKG_NAME"),
        env!("CARGO_PKG_NAME"),
        "ocrete",
        env!("BUILD_REL_DATE")
    );


fn plugin_init(plugin: &gst::Plugin) -> Result<(), glib::BoolError> {
        webrtcsink::register(plugin)?;
        webrtcsrc::register(plugin)?;
        Ok(())
}
    