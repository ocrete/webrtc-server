use gst::glib;
use gst::prelude::*;

mod imp;

glib::wrapper! {
    pub struct WebRtcSrc(ObjectSubclass<imp::WebRtcSrc>) @extends gst::Bin, gst::Element, gst::Object;
}

pub fn register(plugin: &gst::Plugin) -> Result<(), glib::BoolError> {
    gst::Element::register(
        Some(plugin),
        "webrtcsrc",
        gst::Rank::None,
        WebRtcSrc::static_type(),
    )
}