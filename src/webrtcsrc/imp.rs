use gst::glib;

use gst::prelude::*;
use gst::subclass::prelude::*;

use std::sync::Mutex;
use std::cell::Cell;
use once_cell::sync::Lazy;

static CAT: Lazy<gst::DebugCategory> = Lazy::new(|| {
    gst::DebugCategory::new(
        "webrtcsrc",
        gst::DebugColorFlags::empty(),
        Some("WebRTC Source element"),
    )
});

use super::super::peerconnectionserver::{create_routes, WebServer};

pub struct WebRtcSrc {
        webrtcbin: gst::Element,
        webserver: Mutex<Cell<Option<WebServer>>>
}

impl WebRtcSrc {
    fn connect_pad (&self) {
        let obj = self.obj();
        let e = obj.clone().dynamic_cast::<gst::Element>().unwrap();
        let weak = e.downgrade();
        self.webrtcbin.connect_pad_added(move |_, pad| {
            if pad.direction() != gst::PadDirection::Src {
                return;
            }
            let e = match weak.upgrade() {
                None => return,
                Some(e) => e
            };
            let padtrans = pad.property::<gst_webrtc::WebRTCRTPTransceiver>("transceiver");
            let mut gpad: Option<gst::Pad> = None;
            #[allow(clippy::manual_flatten)]
            for p in e.iterate_src_pads() {
                if let Ok(p) = p {
                    let trans: Option<std::ptr::NonNull<gst_webrtc::WebRTCRTPTransceiver>> = unsafe { p.data("transceiver") };
                    if let Some(trans) = trans {
                        let trans = unsafe {trans.as_ptr().as_ref().unwrap().clone() };
                        if padtrans == trans {
                            gpad = Some(p);
                            break;
                        }
                    }
                }
            }
            if let Some(gpad) = gpad {
                gst::debug! (CAT, obj:&e, "Attrached external pad {:?} to internal pad {:?}", gpad, pad);
                let gpad = gpad.downcast::<gst::GhostPad>().unwrap();
                gpad.set_target(Some(pad)).unwrap();
            } else {
                gst::warning! (CAT, obj:&e, "Can't find ghostpad for webrtcbin pad {:?}", pad);
            }
        });
    }

    fn set_caps (&self, webrtcsrc: &super::WebRtcSrc) -> Result<(),()> {
        let mut has_caps = true;

        let filter = gst::Caps::new_simple("application/x-rtp", &[]);

        for p in webrtcsrc.iterate_src_pads() {
            if let Ok(p) = p {

                let caps = p.peer_query_caps(Some(&filter));

                if caps.is_empty() || caps.is_any() {
                    continue;
                }

                let caps = if !caps.is_fixed() {
                    let mut new_caps = gst::Caps::new_empty();
                    let ncapsref = new_caps.get_mut().unwrap();
                    for s in caps.iter() {
                        if ! s.has_field_with_type("clock-rate", glib::types::Type::I32) {
                            continue;
                        }
                        if s.has_field_with_type("encoding-name", glib::types::Type::STRING) {
                            ncapsref.append_structure(s.to_owned());
                        } else if let Ok(list) = s.get::<gst::List>("encoding-name") {
                            for e in list.as_slice() {
                                if let Ok(e) = e.get::<String>() {
                                    let mut s = s.to_owned();
                                    s.set("encoding-name", &e);
                                    ncapsref.append_structure(s);
                                }
                            }
                        }
                    }
                    new_caps
                } else {
                    caps
                };

                gst::debug!(CAT, obj:&p, "Setting codecs: {}", caps);


                unsafe {
                    let trans: Option<std::ptr::NonNull<gst_webrtc::WebRTCRTPTransceiver>> = p.data("transceiver");
                    let trans = trans.unwrap().as_ref();
                    trans.set_codec_preferences(Some(&caps));
                }
            } else {
                has_caps = false;
            }
        }

        if has_caps {
            Ok(())
        } else {
            Err(())
        }
    }
}

#[glib::object_subclass]
impl ObjectSubclass for WebRtcSrc {
    const NAME: &'static str = "WebRtcSrc";
    type Type = super::WebRtcSrc;
    type ParentType = gst::Bin;

    fn new() -> Self {
        let webrtcbin = gst::ElementFactory::make_with_name("webrtcbin", Some("webrtcbin")).unwrap();
        Self { webrtcbin, webserver: Mutex::new(Cell::new(None)) }
    }
}

impl ObjectImpl for WebRtcSrc {
    fn constructed(&self) {
        self.parent_constructed();
        let obj = self.obj();
        let bin = obj.dynamic_cast_ref::<gst::Bin>().unwrap();
        let webrtcbin = self.webrtcbin.upcast_ref::<gst::Element>();
        bin.add (webrtcbin).unwrap();
        self.connect_pad();
    }
}

impl GstObjectImpl for WebRtcSrc {}

impl ElementImpl for WebRtcSrc {
    fn metadata() -> Option<&'static gst::subclass::ElementMetadata> {
        static ELEMENT_METADATA: Lazy<gst::subclass::ElementMetadata> = Lazy::new(|| {
            gst::subclass::ElementMetadata::new(
                "WebRTC Source element",
                "Network/Source",
                "A WebRTC Source element including a web server",
                "Olivier Crête <olivier.crete@collabora.com>",
            )
        });

        Some(&*ELEMENT_METADATA)
    }

    fn pad_templates() -> &'static [gst::PadTemplate] {
        static PAD_TEMPLATES: Lazy<Vec<gst::PadTemplate>> = Lazy::new(|| {
            let caps = gst::Caps::new_simple("application/x-rtp", &[]);
            let sink_pad_template = gst::PadTemplate::new(
                "src_%u",
                gst::PadDirection::Src,
                gst::PadPresence::Request,
                &caps,
            ).unwrap();
            vec![sink_pad_template]
        });

        PAD_TEMPLATES.as_ref()
    }


    fn request_new_pad(&self, templ: &gst::PadTemplate,
        name: Option<&str>, caps: Option<&gst::Caps>) -> Option<gst::Pad> {
            let trans = self.webrtcbin.emit_by_name::<gst_webrtc::WebRTCRTPTransceiver>("add-transceiver", &[&gst_webrtc::WebRTCRTPTransceiverDirection::Recvonly, &caps]);


            let ghostpad = gst::GhostPad::from_template(templ, name.as_deref());

            unsafe {
                ghostpad.set_data("transceiver", trans);
            }

            ghostpad.set_active(true).unwrap();
            self.obj().add_pad(&ghostpad).unwrap();
            let ghostpad = ghostpad.upcast::<gst::Pad>();
            Some(ghostpad)
    }

    fn release_pad(&self, pad: &gst::Pad) {
        unsafe {
            let _: Option<gst_webrtc::WebRTCRTPTransceiver> = pad.steal_data("transceiver");
        }
        pad.set_active(false).ok();
        self.obj().remove_pad(pad).unwrap();
    }


    fn change_state(&self, transition: gst::StateChange) ->
         Result<gst::StateChangeSuccess, gst::StateChangeError> {
        let element = self.obj();

         match transition {
             gst::StateChange::PausedToPlaying=> {
                if self.set_caps(&element).is_err() {
                    return Err(gst::StateChangeError);
                }

                let webrtcbin = self.webrtcbin.clone();
                let routes = create_routes(webrtcbin);

                let webserver = WebServer::new(routes, 3030);
                self.webserver.lock().unwrap().set(Some(webserver));

                self.parent_change_state(transition)
             },
             gst::StateChange::PlayingToPaused => {
                 self.parent_change_state(transition)?;
                 let webserver = self.webserver.lock().unwrap();
                 let webserver = webserver.replace(None);
                 if let Some(webserver) = webserver {
                     webserver.stop();
                 }
                 Ok(gst::StateChangeSuccess::Success)
             },
             _ => self.parent_change_state(transition)
         }    
    }

}

impl BinImpl for WebRtcSrc {}