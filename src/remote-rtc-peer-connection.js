class RemoteRTCPeerConnection extends EventTarget {
        constructor() {
                super();
                this.requests = new Map();
                this._signalingState = 'stable'
                this._connectionState = 'new'
                this._iceConnectionState = 'new'
                this._iceGatheringState = 'new'


                Object.defineProperties(this, {
                        onicecandidate: {
                                writable: true,
                        },
                        onnegotiationneeded: {
                                writable: true,
                        },

                        onsignalingstatechange: {
                                writable: true
                        },
                        signalingState: {
                                get: () => (this._signalingState)
                        },

                        onconnectionchange: {
                                writable: true
                        },
                        connectionState: {
                                get: () => (this._connectionState)
                        },

                        oniceconnectionstate: {
                                writable: true
                        },
                        iceConnectionState: {
                                get: () => (this._iceConnectionState)
                        },

                        onicegatheringstate: {
                                writable: true
                        },
                        iceGatheringState: {
                                get: () => (this._iceGatheringState)
                        }
                });

        }

        onServerClose = (event) => {
                console.log(event);
                for (let channels in this.requests.values()) {
                        console.log("id: " + id);
                        console.log("channel: " + channel);
                        var error = JSON.stringify({ "error": "server closed" });
                        channel.port2.postMessage(error, []);
                }
                this.requests = new Map();
        }

        onServerError = (event) => {
                for (let channel in this.requests.values()) {
                        var error = JSON.stringify({ "error": "servererror" });
                        channel.port2.postMessage(error, []);
                }
                this.requests = new Map();
        }

        updateProperty(prop, value) {
                if (!this.hasOwnProperty('_' + prop)) {
                        return;
                }
                this['_' + prop] = value;
                var evname = prop.toLowerCase() + 'change';
                var signal = 'on' + evname;
                if (this.hasOwnProperty(signal)) {
                        var event = new Event(evname);
                        if (typeof this[signal] == 'function') {
                                this[signal](event);
                        }
                        this.dispatchEvent(event);
                }
        }

        onServerMessage = (event) => {
                var msg = JSON.parse(event.data);

                if ('id' in msg) {
                        if (msg.id in this.requests) {
                                this.requests[msg.id].port2.postMessage(event.data, []);
                                delete this.requests[msg.id];
                        } else {
                                throw new Error("Invalid request id: ${msg.id}");
                        }
                } else if ('event' in msg) {
                        if (msg.event == 'onicecandidate') {
                                if (typeof this.onicecandidate == 'function') {
                                        var ev = new Event('icecandidate');
                                        ev.candidate = msg.object;
                                        this.onicecandidate(ev);
                                }
                        } else if (msg.event == 'onnegotiationneeded') {
                                if (typeof this.onnegotiationneeded == 'function') {
                                        this.onnegotiationneeded(new Event('negotiationneeded'));
                                }
                        } else {
                                console.log("Got event " + msg.event)
                        }

                } else if ('property' in msg) {
                        if (msg.property == 'signaling-state') {
                                this.updateProperty('signalingState', msg.value);
                        } else if (msg.property == 'ice-connection-state') {
                                this.updateProperty('iceConnectionState', msg.value);
                        } else if (msg.property == 'ice-gathering-state') {
                                this.updateProperty('iceGatheringState', msg.value);
                        } else if (msg.property == 'connection-state') {
                                this.updateProperty('connectionState', msg.value);
                        } else {
                                console.log("Got unknown property " + msg.property)
                        }
                } else {
                        console.log("Invalid message");
                }
        }

        _sendRequest(request, args = null) {
                var id = Math.random().toString(36).substring(7);
                return new Promise((resolve, reject) => {
                        var chan = this.requests[id] = new MessageChannel();
                        chan.port1.onmessageerror = (e) => reject(DOMException("Message port error"));
                        chan.port1.onmessage = (e) => {
                                var msg = JSON.parse(e.data)
                                if (msg) {
                                        var value = JSON.parse(msg.value);
                                        if (value) {
                                                if (msg.success) {
                                                        resolve(value);
                                                } else {
                                                        reject(value)
                                                }
                                        } else {
                                                reject(DOMException("Coulds not parse message: " + msg.value));
                                        }
                                } else {
                                        reject(DOMException("Couldn't parse message"));
                                }
                        };
                        var json = JSON.stringify({ id: id, request: request, object: args });
                        this.ws_conn.send(json);
                });
        }


        connect(ws_protocol, ws_server, ws_port) {
                var ws_url = ws_protocol + "//" + ws_server + ':' + ws_port + "/webrtcbin";

                this.ws_conn = new WebSocket(ws_url);

                this.ws_conn.addEventListener('message', this.onServerMessage);
                this.ws_conn.addEventListener('close', this.onServerClose);

                return new Promise((resolve, reject) => {
                        this.ws_conn.addEventListener('open', (event) => {
                                this.ws_conn.addEventListener('error', this.onServerError);
                                resolve(this);
                        });
                        this.ws_conn.addEventListener('error', (event) => {
                                reject(event);
                                this.ws_conn.addEventListener('error', this.onServerError);
                        });
                });
        }

        async createOffer() {
                return await this._sendRequest('CreateOffer');
        }

        async createAnswer() {
                return await this._sendRequest('CreateAnswer');
        }

        async setLocalDescription(sessionDescription) {
                return await this._sendRequest('SetLocalDescription', sessionDescription)
        }

        async setRemoteDescription(sessionDescription) {
                return await this._sendRequest('SetRemoteDescription', sessionDescription)
        }

        async addIceCandidate(candidate) {
                return await this._sendRequest('AddIceCandidate', candidate)
        }
};