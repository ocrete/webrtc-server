use gst::glib;
use gst::prelude::*;

mod imp;

glib::wrapper! {
    pub struct WebRtcSink(ObjectSubclass<imp::WebRtcSink>) @extends gst::Bin, gst::Element, gst::Object;
}

pub fn register(plugin: &gst::Plugin) -> Result<(), glib::BoolError> {
    gst::Element::register(
        Some(plugin),
        "webrtcsink",
        gst::Rank::None,
        WebRtcSink::static_type(),
    )
}