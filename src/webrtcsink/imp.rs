use gst::glib;

use gst::prelude::*;
use gst::subclass::prelude::*;
//use gst::{gst_debug, gst_info};

use std::sync::Mutex;
use std::cell::Cell;
use once_cell::sync::Lazy;

use super::super::peerconnectionserver::{create_routes, WebServer};

const DEFAULT_PORT: u16 = 3030;

#[derive(Debug, Clone, Copy)]
struct Settings {
    port: u16,
}

impl Default for Settings {
    fn default() -> Self {
        Settings {
            port: DEFAULT_PORT,
        }
    }
}

pub struct WebRtcSink {
    webrtcbin: gst::Element,
    webserver: Mutex<Cell<Option<WebServer>>>,
    settings: Mutex<Settings>
}

impl WebRtcSink {}

#[glib::object_subclass]
impl ObjectSubclass for WebRtcSink {
    const NAME: &'static str = "WebRtcSink";
    type Type = super::WebRtcSink;
    type ParentType = gst::Bin;

    fn new() -> Self {
        let webrtcbin = gst::ElementFactory::make_with_name("webrtcbin", Some("webrtcbin")).unwrap();
        Self {
            webrtcbin,
            webserver: Mutex::new(Cell::new(None)),
            settings: Mutex::new(Settings::default())
        }
    }
}

impl ObjectImpl for WebRtcSink {
    fn constructed(&self) {
        self.parent_constructed();
        let webrtcsink = self.obj();
        let bin = webrtcsink.dynamic_cast_ref::<gst::Bin>().unwrap();
        let webrtcbin = self.webrtcbin.upcast_ref::<gst::Element>();
        bin.add (webrtcbin).unwrap();
    }

    fn properties() -> &'static [glib::ParamSpec] {
        // Metadata for the properties
        static PROPERTIES: Lazy<Vec<glib::ParamSpec>> = Lazy::new(|| {
            vec![
                glib::ParamSpecUInt::new(
                    "port",
                    "Port",
                    "WebRTC server port",
                    0,
                    65535,
                    DEFAULT_PORT.into(),
                    glib::ParamFlags::READWRITE | gst::PARAM_FLAG_MUTABLE_PAUSED,
                ),
            ]
        });

        PROPERTIES.as_ref()
    }

    fn set_property(
        &self,
        _id: usize,
        value: &glib::Value,
        pspec: &glib::ParamSpec,
    ) {
        match pspec.name() {
            "port" => {
                let port: u32 = value.get().unwrap();
                let mut settings = self.settings.lock().unwrap();
                settings.port = port as u16;
            }
            _ => unimplemented!(),
        }
    }

    fn property(&self, _id: usize, pspec: &glib::ParamSpec) -> glib::Value {
        match pspec.name() {
            "port" => {
                let settings = self.settings.lock().unwrap();
                let port = settings.port as u32;
                port.to_value()
            }
            _ => unimplemented!(),
        }
    }
}

impl GstObjectImpl for WebRtcSink {}

impl ElementImpl for WebRtcSink {
    fn metadata() -> Option<&'static gst::subclass::ElementMetadata> {
        static ELEMENT_METADATA: Lazy<gst::subclass::ElementMetadata> = Lazy::new(|| {
            gst::subclass::ElementMetadata::new(
                "WebRTC Sink element",
                "Network/Sink",
                "A WebRTC Sink element including a web server",
                "Olivier Crête <olivier.crete@collabora.com>",
            )
        });

        Some(&*ELEMENT_METADATA)
    }

    fn pad_templates() -> &'static [gst::PadTemplate] {
        static PAD_TEMPLATES: Lazy<Vec<gst::PadTemplate>> = Lazy::new(|| {
            let caps = gst::Caps::new_simple("application/x-rtp", &[]);
            let sink_pad_template = gst::PadTemplate::new(
                "sink_%u",
                gst::PadDirection::Sink,
                gst::PadPresence::Request,
                &caps,
            ).unwrap();
            vec![sink_pad_template]
        });

        PAD_TEMPLATES.as_ref()
    }

    fn request_new_pad(&self, templ: &gst::PadTemplate,
        name: Option<&str>, caps: Option<&gst::Caps>) -> Option<gst::Pad> {
            let name_templ = templ.name_template();
            let subtempl = self.webrtcbin.element_class().pad_template(&name_templ)?;
            let pad = self.webrtcbin.request_pad(&subtempl, name.as_deref(), caps)?;

            let trans = pad.property::<gst_webrtc::WebRTCRTPTransceiver>("transceiver");
            trans.set_direction(gst_webrtc::WebRTCRTPTransceiverDirection::Sendonly);

            let name = pad.name();
            let ghostpad = gst::GhostPad::from_template_with_target(templ, Some(&name), &pad).unwrap();
            ghostpad.set_active(true).unwrap();
            let element = self.obj();
            element.add_pad(&ghostpad).unwrap();
            let ghostpad = ghostpad.upcast::<gst::Pad>();
            Some(ghostpad)
    }

    fn release_pad(&self, pad: &gst::Pad) {
        let ghostpad = pad.clone().downcast::<gst::GhostPad>().unwrap();

        let target = ghostpad.target();
        if let Some(target) = target {
            self.webrtcbin.release_request_pad(&target);
        }

        let element = self.obj();
        element.remove_pad(pad).unwrap();
        pad.set_active(false).ok();
    }

    fn change_state(&self, transition: gst::StateChange) ->
         Result<gst::StateChangeSuccess, gst::StateChangeError> {
         match transition {
             gst::StateChange::PausedToPlaying => {
                let webrtcbin = self.webrtcbin.clone();
                let routes = create_routes(webrtcbin);
                let port = self.settings.lock().unwrap().port;
                let webserver = WebServer::new(routes, port);
                self.webserver.lock().unwrap().set(Some(webserver));

                self.parent_change_state(transition)
             },
             gst::StateChange::PlayingToPaused => {
                 self.parent_change_state(transition)?;
                 let webserver = self.webserver.lock().unwrap();
                 let webserver = webserver.replace(None);
                 if let Some(webserver) = webserver {
                     webserver.stop();
                 }
                 Ok(gst::StateChangeSuccess::Success)
             },
             _ => self.parent_change_state(transition)
         }    
    }

}

impl BinImpl for WebRtcSink {}